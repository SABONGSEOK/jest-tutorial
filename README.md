<h1>TDD_JEST</h1>
TDD Jest study

<h2>[STEP1] Jest 설치 및 간단한 테스트 작성</h2>
<h3>- Jest 설치 및 실행</h3>
```plainText
설치: `npm i jest --save-dev`
실행: `npm run test` <-- package.json scripts 참고
원하는 테스트 파일만 실행: 
    `npm run test 테스트파일명` or `npm run test 폴더명`
```
<h3>- 기본 테스트 스크립트</h3>
```plainText
파일명 확장자를  `.spec.js` 또는 `.test.js` 로 설정
or
`__tests__` 폴더를 생성하여 내부에 테스트 스크립트 작성
```

<h2>[STEP2] Matchers - 자바스크립트 테스트 프레임워크</h2>
```plainText
Matchers: 테스트 예상결과를 지정하는 함수
toBe: 값 일치
toEqual: 객체 내부 값 일치
toStrictEqual: 객체 요소까지 일치

값의 상태 체크
toBeNull
toBeUndefined
toBeDefined

값의 Boolean값 체크
toBeTruthy
toBeFalsy

값의 비교
toBeGreaterThan: 초과
toBeGreaterThanOrEqual:이상
toBeLessThan:미만
toBeLessThanOrEqual:이하
toBeCloseTo: 근사치.. 프로그램상 이진법 계산 때문에 발생하는 경우

문자열 확인
toMatch: 문자열 내에 찾는 문자열이 포함되어있음(정규표현식 가능)

배열 요소 확인
toContain: 배열 내에 요소값이 포함되어있는지 체크

에러 체크
toThrow: 예외가 발생했을때 패스
특정 에러를 체크할 경우 `toThrow('에러내용')`을 통해 확인 가능

````

<h2>[STEP3] 비동기 테스트</h2>

<h3>- [콜백패턴] 잘못된 비동기 테스트</h3>
```javascript
/*
* 스크립트가 끝나고 비동기 처리 없이 바로 결과 반환.
*
* 실행결과: 잘못된 결과.
* - pass 3초 후 받아온 이름은 `Mike`야
* - pass 0 + 1은 1이야
* */

//fn.spec.js
test("3초 후 받아온 이름은 `Mike`야", () => {
  function cb(name) {
    expect(name).toBe("Tom");
  }
  fn.getName(cb);
});

test("0 + 1은 1이야", () => {
    expect(fn.add(0, 1)).toBe(1);
});
````

<h3>- [콜백패턴] 정상적 비동기 테스트</h3>
```javascript
/*
* test함수에서 doneCallback파라미터를 반환받아 
* 테스트 완료 후 실행시킴.
* doneCallback함수를 실행하지 않으면 에러 발생.
* 
* 실행결과: 비동기 처리가 완료된 후, 다음 테스트 진행.
* - fail 3초 후 받아온 이름은 `Mike`야
* - pass 0 + 1은 1이야
* */
test("3초 후 받아온 이름은 `Mike`야", (done) => {
  function cb(name) {
    expect(name).toBe("Tom");
    done();
  }
  fn.getName(cb);
});

test("0 + 1은 1이야", () => {
expect(fn.add(0, 1)).toBe(1);
});

````

<h3>- [Promise패턴] 비동기 테스트</h3>
```plainTest
Promise 비동기 처리시, jset에서 자동으로 대기함
단, 비동기 함수를 호출할 때 return 타입으로 반환해야 정상 동작함

방식1) 비동기 함수 실행 후 결과에서 예상값 테스트
return fn.getAge().then((age) => {
    expect(age).toBe(30);
});

방식2) Matcher(resolves, rejects)를 통한 처음부터 예상값 테스트
return expect(fn.getAge()).resolves.toBe(30);
````

<h3>- [async await패턴] 비동기 테스트</h3>
```plainTest
방식1) await를 통해 비동기 결과 반환값을 받아서 테스트
const gender = await fn.getGender();
expect(gender).toBe("M");
  
방식2) Matcher(resolves, rejects)를 통한 처음부터 예상값 테스트
await expect(fn.getGender()).resolves.toBe("M");
```

<h2>[STEP4] 테스트 전후 작업</h2>
```plainText
dscribe()
beforeEach(fn): 각각의 테스트 전 작업
afterEach(fn): 각각의 테스트 후 작업

beforeAll(fn): 첫 테스트를 진행하기 전, 1번 작업
afterAll(fn): 모든 테스트를 마친 후, 1번 작업
```
<h3>- 테스트 실행 순서</h3>
```plainText
[예시 구조]
beforeAll()                         //1
beforeEach()                        //2, 6
afterEach()                         //4, 10
afterAll()                          //12

test("테스트1")                      //3

describe("테스트 구분", () {
    beforeAll()                     //5
    beforeEach()                    //7
    afterEach()                     //9
    afterAll()                      //11
    
    test("테스트 2")                 //8
})
```

<h3>- 선택적 테스트</h3>
[jest docs](https://jestjs.io/docs/api)
``` 
여러 테스트 중 특정한 테스트만 진행하는 방법
- test.skip: 해당 테스트를 실행하지 않고 진행
- test.only: 해당 테스트만 진행
```

<h2>[STEP5] 목 함수(mock Functions)</h2>
[jest mock](https://jestjs.io/docs/mock-function-api)
```
실제 데이터를 가져오거나 커스텀이 힘든 경우,
가짜 데이터를 생성하여 테스트하는 방법
jest.fn(): 목 함수 생성
```
<h3>- mock 함수 기본</h3>
```PlainText
const mockFn = jest.fn(); // mock함수생성
```


<h4>mockFn.mock.calls</h4>
```
mockFn 함수가 호출되었을때 받아온 파라미터를 배열타입으로 반환
배열의 길이로 함수 호출 횟수 파악 가능
ex)
    mockFn();
    mockFn(1);
    console.log(mockFn.mock.call);
    
    결과: [[], [1]]
```

<h3>- mock 함수 사용법</h3>
```
mockFn 파라미터로 수식을 넣어서 함수처럼 사용가능
mockFn.mock.calls (ex1 참고)
    mockFn 호출시 배열타입으로 넘어온 파라미터 반환
    mockFn의 호출 횟수 파악 가능
mockFn.mock.results (ex2 참고)
    jest.fn()내부에 함수 기능을 입력하여 반환 결과를 배열로 반환
반환값 설정 (ex3 참고)
    mockFn.mockReturnValueOnce(value): 1회 반환
    mockFn.mockReturnValue(value): 디폴트 반환
    mockFn.mockResolvedValueOnce(value): 비동기 1회 반환
    mockFn.mockResolvedValue(value): 비동기 디폴트 반환
예상값 체크
    expect(mockFn).toBeCalled(): 호출된 적이 있는가
    expect(mockFn).toBeCalledTimes(value): value만큼 호출되었는가
    expect(mockFn).toBeCalledWith(values): values 파라미터를 받은 호출이 있는가
    expect(mockFn).lastCalledWith(values): 마지막 호출 파라미터가 values인가
```
```Javascript
// ex1) mockFn.mock.calls
// 결과: [[11], [21], [31]]
function forEachAdd1(arr) {
    arr.forEach((num) => {
        mockFn(num + 1);
    });
}

forEachAdd1([10, 20, 30]);

test("mock함수는 3회 실행됨", () => {
    expect(mockFn.mock.calls.length).toBe(3);
});
test("목 함수 반환 값은 11, 21, 31", () => {
    expect(mockFn.mock.calls[0][0]).toBe(11);
    expect(mockFn.mock.calls[1][0]).toBe(21);
    expect(mockFn.mock.calls[2][0]).toBe(31);
});
```
```Javascript
// ex2) mockFn.mock.results
// 결과: 
// [
//  {type: 'return', value: 11},
//  {type: 'return', value: 21},
//  {type: 'return', value: 31}
// ]
const mockFn = jest.fn((num) => num + 1);
mockFn(10);
mockFn(20);
mockFn(30);
test("mock함수는 3회 실행됨", () => {
    console.log(mockFn.mock.results);
    expect(mockFn.mock.results.length).toBe(3);
});
```
```Javascript
// ex3) mockReurnValue, mockReturnValueOnce
// 결과
// [
//     { type: 'return', value: 'first call' },
//     { type: 'return', value: 'second call' },
//     { type: 'return', value: 'default' },
//     { type: 'return', value: 'default' },
//     { type: 'return', value: 'default' }
// ]

const myMockFn = jest
    .fn()
    .mockReturnValue("default")
    .mockReturnValueOnce("first call")
    .mockReturnValueOnce("second call");

myMockFn();
myMockFn();
myMockFn();
myMockFn();
myMockFn();
test("mockReturnValue, mockReturnValueOnce", () => {
    console.log(myMockFn.mock.results);
    expect(1).toBe(1);
});
```