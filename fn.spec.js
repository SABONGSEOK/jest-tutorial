// import { fn } from "./fn.js";
const fn = require("./fn");

/*[STEP1]*/
describe("[STEP1] jest basic", () => {
  test("1은 1이야.", () => {
    expect(1).toBe(1);
  });

  test("2+3=5", () => {
    expect(fn.add(2, 3)).toBe(5);
  });

  test("3+3 != 5", () => {
    expect(fn.add(3, 3)).not.toBe(5);
  });
});

/*[STEP2]*/
describe("[STEP2] Matchers", () => {
  //객체는 재귀적으로 탐색하기 때문에 `toBe`를 사용할 시 에러남
  test("이름과 나이를 전달받아서 객체 반환해줘.", () => {
    expect(fn.makeUser("Mike", 30)).toBe({ name: "Mike", age: 30 });
  });

  //객체를 확인할시 `toEqual` 또는 `toStrictEqual`을 사용해야 함
  test("이름과 나이를 전달받아서 객체 반환해줘.", () => {
    expect(fn.makeUser("Mike", 30)).toEqual({ name: "Mike", age: 30 });
  });

  //`toStrictEqual`는 객체의 요소 구성까지 같은지 체크함
  test("이름과 나이를 전달받아서 객체 반환해줘.", () => {
    expect(fn.makeUser("Mike", 30)).toStrictEqual({ name: "Mike", age: 30 });
  });

  //값의 상태 체크
  //`toBeNull`
  test("null은 null이야.", () => {
    expect(null).toBeNull();
  });
  //`toBeUndefined`
  let a;
  test("a는 undefiend야.", () => {
    expect(a).toBeUndefined();
  });
  //`toBeDefined`
  let b = "b";
  test("b는 defiend야.", () => {
    expect(b).toBeDefined();
  });

  //값의 Boolean값 체크
  //`toBeTruthy`
  test("비어있지 않은 문자열은 true야.", () => {
    expect(fn.add("Hello", "World")).toBeTruthy();
  });
  //`toBeFalsy`
  test("빈 문자열은 false야.", () => {
    expect(fn.add("", "")).toBeFalsy();
  });

  //값의 비교
  //`toBeGreaterThan`
  //`toBeGreaterThanOrEqual`
  //`toBeLessThan`
  //`toBeLessThanOrEqual`
  test("ID는 10자 이하여야한다.", () => {
    const id = "THE_BLACK";
    expect(id.length).toBeLessThanOrEqual(10);
  });
  //`toBeCloseTo`: 근사치
  test("0.1 더하기 0.2는 0.3이야", () => {
    expect(fn.add(0.1, 0.2)).toBeCloseTo(0.3);
  });

  //문자열 확인
  test("문자열 내에 소문자`o`가 있다", () => {
    expect("Hello World").toMatch(/o/);
  });

  test("문자열 내에 `o`가 있다", () => {
    expect("Hello World").toMatch(/O/i);
  });

  //배열 요소 확인
  //`toContain`
  test("배열 내에 `banana`가 있다", () => {
    const fruits = ["apple", "banana", "cherry"];
    expect(fruits).toContain("banana");
  });

  //예외 처리
  //`toThrow`
  test("에러가 발생하나?", () => {
    expect(() => fn.throwErr()).toThrow();
  });

  test("에러가 aa 인가?", () => {
    expect(() => fn.throwErr()).toThrow("aa");
  });
});

/*[STEP3] 비동기 테스트*/
describe("[STEP3] 비동기 테스트", () => {
  //콜백 패턴
  //test 함수 설정시 함수에 done파라미터를 받지 않는 경우 딜레이되기 전에 실행이 완료됨. 그 후 딜레이 후 결과 또 받음
  //done함수를 실행하지 않는 경우 에러 발생.
  test("3초 후 받아온 이름은 `Mike`야", (done) => {
    function cb(name) {
      expect(name).toBe("Mike");
      done();
    }

    fn.getName(cb);
  });

  test("0 + 1은 1이야", () => {
    expect(fn.add(0, 1)).toBe(1);
  });

  //Promise패턴
  //jest에서 자동으로 비동기 처리를 대기해줌. 단, 비동기 함수는 return 처리를 해야 정상 작동 함
  test("3초 후 받아온 나이는 30야", () => {
    /*!//기본적인 promise 처리 방식
    return fn.getAge().then((age) => {
      expect(age).toBe(30);
    });*/

    //Matcher(resolves, rejects)를 통한 Promise 처리방식
    return expect(fn.getAge()).resolves.toBe(30);
  });

  test("1 + 1은 2야", () => {
    expect(fn.add(1, 1)).toBe(2);
  });

  //async await 비동기 처리
  test("3초 후 받아온 성별은 `M`", async () => {
    //기본적인 async await 테스트 방식
    // const gender = await fn.getGender();
    // expect(gender).toBe("M");

    //Matcher를 통한 테스트 방식
    await expect(fn.getGender()).resolves.toBe("M");
  });
});

/*[STEP4] 테스트 전후 작업*/
describe("[STEP4] 테스트 전후 작업", () => {
  //각각의 테스트 전 초기화
  let num = 0;
  beforeEach(() => {
    num = 0;
  });

  test("0 더하기 1 은 1이야.", () => {
    num = fn.add(num, 1);
    expect(num).toBe(1);
  });

  test("0 더하기 2 은 2이야.", () => {
    num = fn.add(num, 2);
    expect(num).toBe(2);
  });

  test("0 더하기 3 은 3이야.", () => {
    num = fn.add(num, 3);
    expect(num).toBe(3);
  });

  test("0 더하기 4 은 4이야.", () => {
    num = fn.add(num, 4);
    expect(num).toBe(4);
  });

  //비동기 전 처리
  let user = null;
  beforeAll(async () => {
    user = await fn.connectDB();
  });

  afterAll(() => {
    user = null;
    return fn.disconnectDB();
  });

  test("[STEP4] 이름: MIKE", () => {
    expect(user.name).toBe("Mike");
  });

  test("[STEP4] 나이: 30", () => {
    expect(user.age).toBe(30);
  });

  test("[STEP4] 성별: 남성", () => {
    expect(user.gender).toBe("male");
  });

  //테스트 순서
  beforeEach(() => console.log("밖 beforeEach"));
  beforeAll(() => console.log("밖 beforeAll"));
  afterEach(() => console.log("밖 afterEach"));
  afterAll(() => console.log("밖 afterAll"));

  test("밖 테스트1", () => {
    console.log("밖 테스트1");
    expect(0).toBe(0);
  });

  describe("내부 테스트", () => {
    beforeEach(() => console.log("안 beforeEach"));
    beforeAll(() => console.log("안 beforeAll"));
    afterEach(() => console.log("안 afterEach"));
    afterAll(() => console.log("안 afterAll"));

    test("안 테스트1", () => {
      console.log("안 테스트1");
      expect(0).toBe(0);
    });
  });
});

/*[STEP5] mock 함수*/
describe("[STEP5] mock Functions", () => {
  let mockFn = jest.fn();
  //mock 함수 기본 사항
  mockFn();
  mockFn([1], { t: "t" });
  test("mock 기본", () => {
    console.log(mockFn.mock.calls);
    expect(1).toBe(1);
  });

  //목함수 사용법
  // ex) 숫자배열을 입력받아 값을 1씩 증가
  function forEachAdd1(arr) {
    arr.forEach((num) => {
      mockFn(num + 1);
    });
  }
  forEachAdd1([10, 20, 30]);

  test("mock함수는 3회 실행됨", () => {
    expect(mockFn.mock.calls.length).toBe(3);
  });
  test("목 함수 반환 값은 11 21 31", () => {
    expect(mockFn.mock.calls[0][0]).toBe(11);
    expect(mockFn.mock.calls[1][0]).toBe(21);
    expect(mockFn.mock.calls[2][0]).toBe(31);
  });

  //jest.fn(함수 기능) 사용법법
  mockFn = jest.fn((num) => num + 1);
  mockFn(10);
  mockFn(20);
  mockFn(30);
  test("mock함수는 3회 실행됨", () => {
    console.log(mockFn.mock.results);
    expect(mockFn.mock.results.length).toBe(3);
  });

  const myMockFn = jest
    .fn()
    .mockReturnValue("default")
    .mockReturnValueOnce("first call")
    .mockReturnValueOnce("second call");

  myMockFn();
  myMockFn();
  myMockFn();
  myMockFn();
  myMockFn();
  test("mockReturnValue, mockReturnValueOnce", () => {
    console.log(myMockFn.mock.results);
    expect(1).toBe(1);
  });
});
